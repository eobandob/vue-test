import Vue from "vue";
import VueRouter from "vue-router";
import About from "@/views/About";
import Home from "@/views/Home";

Vue.use(VueRouter);

const routes = [
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
